Class BO.Reservation Extends Ens.BusinessOperation
{

Property Adapter As EnsLib.File.OutboundAdapter;

Parameter ADAPTER = "EnsLib.File.OutboundAdapter";

Parameter INVOCATION = "Queue";

Method creerReservation(pRequest As msg.creerReservationDmde, Output pResponse As msg.creerReservationRpse) As %Status
{
    set pResponse = ##class(msg.creerReservationRpse).%New()

    //top 1 is required to recover only 1 license plate
    set sql = "SELECT top 1 plaque from data.Voiture WHERE categorie = '"_pRequest.categorie_"' AND plaque not in (SELECT voiture->plaque FROM data.Reservation WHERE voiture->categorie = '"_pRequest.categorie_"' AND ((dateDebut >= '"_pRequest.dateDebut_"' AND dateDebut <= '"_pRequest.dateFin_"') OR (dateFin >= '"_pRequest.dateDebut_"' AND dateFin <= '"_pRequest.dateFin_"') OR (dateDebut <= '"_pRequest.dateDebut_"' AND dateFin >= '"_pRequest.dateFin_"') OR (dateDebut >= '"_pRequest.dateDebut_"' AND dateFin <= '"_pRequest.dateFin_"')))"

    set statement=##class(%SQL.Statement).%New()
	do statement.%Prepare(sql)
	set SQLrequest = statement.%Execute()
    do SQLrequest.%Next()
    set plaque = SQLrequest.%Get("plaque")

    if (##class(data.Voiture).consulterParPlaqueExists(plaque, .idVoiture)){
    set dataVoiture = ##class(data.Voiture).%OpenId(idVoiture)
    set reservation = ##class(data.Reservation).%New()
    set reservation.voiture = dataVoiture
    set reservation.dateDebut = pRequest.dateDebut
    set reservation.dateFin = pRequest.dateFin
    set tsc = reservation.%Save()
    if (tsc){
        set pResponse.codeRetour = "OK"
        set pResponse.plaque = plaque
    }
    else {
        set pResponse.codeRetour = "KO"
    }
    }
    else {
        set pResponse.codeRetour = "KO"
    }
    
    
    
    Quit $$$OK
}

Method verifReservation(pRequest As msg.creerVerifReservationDmde, Output pResponse As msg.creerVerifReservationRpse) As %Status
{
    set pResponse = ##class(msg.creerVerifReservationRpse).%New()
    set sql = "SELECT voiture FROM data.Reservation WHERE voiture->plaque = '"_pRequest.plaque_"' AND (dateDebut >= GETDATE() OR dateFin >= GETDATE())"
    set statement=##class(%SQL.Statement).%New() 
	do statement.%Prepare(sql)
	set SQLrequest = statement.%Execute()
    do SQLrequest.%Next()
    if (SQLrequest.%Get("voiture")){
        set pResponse.codeRetour = "KO"
    }else{
        set pResponse.codeRetour = "OK"
    }
    
    Quit $$$OK
}

XData MessageMap
{
<MapItems>
    <MapItem MessageType="msg.creerReservationDmde">
        <Method>creerReservation</Method>
    </MapItem>
    <MapItem MessageType="msg.creerVerifReservationDmde">
    <Method>verifReservation</Method>
    </MapItem>
</MapItems>
}

}
